package com.skillbox.statistics.controller;

import com.skillbox.statistics.base.BaseTest;
import com.skillbox.statistics.config.AppKafkaProperties;
import com.skillbox.statistics.domain.Event;
import com.skillbox.statistics.repos.EventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@EnableConfigurationProperties
class EventControllerTest extends BaseTest {
    @Autowired
    private AppKafkaProperties appKafkaProperties;
    private Event commentEvent;
    private Event postEvent;
    private Event subscriptionEvent;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    private WebApplicationContext webContext;
    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
        commentEvent = new Event();
        commentEvent.setUuid(UUID.randomUUID());
        commentEvent.setDateTime(LocalDateTime.now());
        commentEvent.setEventInitiator("saklhdlajhshfahsfhaj");
        commentEvent.setObjectId("rtasiodasydoaysoda");
        commentEvent.setObjectType("Comment");

        postEvent = new Event();
        postEvent.setUuid(UUID.randomUUID());
        postEvent.setDateTime(LocalDateTime.now());
        postEvent.setEventInitiator("162361206401602");
        postEvent.setObjectId("09898706865");
        postEvent.setObjectType("Post");

        subscriptionEvent = new Event();
        subscriptionEvent.setUuid(UUID.randomUUID());
        subscriptionEvent.setDateTime(LocalDateTime.now());
        subscriptionEvent.setEventInitiator("asgdalgsfhaglsgfqwyeq");
        subscriptionEvent.setObjectId("156289564124");
        subscriptionEvent.setObjectType("Subscription");

        eventRepository.saveAll(Arrays.asList(commentEvent,postEvent,subscriptionEvent));
    }

    @Test
    void getEventByPostId() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/statistics/getEventByPostId/{id}", postEvent.getUuid()));

        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.uuid").value(postEvent.getUuid().toString()));
    }

    @Test
    void getEventByCommentId() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/statistics/getEventByCommentId/{id}", commentEvent.getUuid()));
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.uuid").value(commentEvent.getUuid().toString()));
    }

    @Test
    void getEventBySubscriptionId() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/statistics/getEventBySubscriptionId/{id}", subscriptionEvent.getUuid()));
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.uuid").value(subscriptionEvent.getUuid().toString()));
    }
}