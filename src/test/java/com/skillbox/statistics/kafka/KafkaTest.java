package com.skillbox.statistics.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skillbox.statistics.base.BaseTest;
import com.skillbox.statistics.config.AppKafkaProperties;
import com.skillbox.statistics.dto.EventRecord;
import com.skillbox.statistics.service.EventService;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Testcontainers
@EnableConfigurationProperties(AppKafkaProperties.class)
class KafkaTest extends BaseTest {
    private static final Logger log = LoggerFactory.getLogger(KafkaTest.class);
    private static final String STATISTICS_DLT = "statistics.DLT";
    private static final String TOPIC = "statistics";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private KafkaOperations<String, EventRecord> operations;

    @Autowired
    private EventService eventService;

    private static KafkaConsumer<String, String> kafkaConsumer;

    @BeforeAll
    static void setUp() {
        kafkaConsumer = getKafkaConsumer();
        kafkaConsumer.subscribe(List.of(STATISTICS_DLT));
    }

    @AfterAll
    static void close() {
        // Close the consumer before shutting down Testcontainers Kafka instance
        kafkaConsumer.close();
    }

    @Test
    void should_not_produce_onto_dlt_for_ok_message() throws JsonProcessingException {
        // Send in valid order
        EventRecord event = EventRecord.builder()
                .uuid(UUID.randomUUID())
                .eventInitiator("saklhdlajhshfahsfhaj")
                .dateTime(LocalDateTime.now())
                .objectId("rtasiodasydoaysoda")
                .objectType("Comment")
                .build();
        operations.send(TOPIC, event.uuid().toString(), event)
                .whenCompleteAsync((result, ex) -> {
                    if (ex == null) {
                        log.info("Success: {}", result);
                    }
                    else {
                        log.error("Failure ", ex);
                    }
                });

        // Verify no message was produced onto Dead Letter Topic
        assertThrows(
                IllegalStateException.class,
                () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, STATISTICS_DLT, Duration.ofSeconds(5)),
                "No records found for topic");
        assertNotNull(eventService.getEventByCommentId(event.uuid()));
    }

    @Test
    void should_produce_onto_dlt_for_bad_message() throws JsonProcessingException {
        // UUID with wrong pattern, validation will fail
        EventRecord event = EventRecord.builder()
                .eventInitiator("saklhdlajhshfahsfhaj")
                .dateTime(LocalDateTime.now())
                .objectId("rtasiodasydoaysoda")
                .objectType("Comment")
                .build();
        operations.send(TOPIC, UUID.randomUUID().toString(), event)
                .whenCompleteAsync((result, ex) -> {
                    if (ex == null) {
                        log.info("Success: {}", result);
                    }
                    else {
                        log.error("Failure ", ex);
                    }
                });

        // Verify message produced onto Dead Letter Topic
        assertDoesNotThrow(
                () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, STATISTICS_DLT, Duration.ofSeconds(5)), "No records found for topic");
    }
}
