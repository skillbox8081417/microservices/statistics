package com.skillbox.statistics.repos;

import com.skillbox.statistics.domain.Event;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface EventRepository extends MongoRepository<Event, UUID> {
    Event findByObjectTypeAndUuid (String objectType, UUID uuid);
}
