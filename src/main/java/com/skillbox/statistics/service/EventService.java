package com.skillbox.statistics.service;

import com.skillbox.statistics.domain.Event;
import com.skillbox.statistics.repos.EventRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class EventService {
    private final EventRepository eventRepository;

    public void createEvent(Event event) {
        eventRepository.save(event);
    }

    public Event getEventByPostId(UUID postUuid) {
        var postEvent = Optional.ofNullable(eventRepository.findByObjectTypeAndUuid("Post", postUuid));
        return postEvent.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Event getEventByCommentId(UUID commentUuid) {
        var commentEvent = Optional.ofNullable(eventRepository.findByObjectTypeAndUuid("Comment", commentUuid));
        return commentEvent.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Event getEventBySubscriptionId(UUID subUuid) {
        var subEvent = Optional.ofNullable(eventRepository.findByObjectTypeAndUuid("Subscription", subUuid));
        return subEvent.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
