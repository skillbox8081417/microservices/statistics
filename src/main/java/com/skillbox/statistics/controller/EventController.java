package com.skillbox.statistics.controller;

import com.skillbox.statistics.domain.Event;
import com.skillbox.statistics.service.EventService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/statistics")
public class EventController {
    private final EventService eventService;

    @GetMapping("/getHelloFromStatisticsApp")
    @Operation(summary = "Get event for post by id")
    public String getHello() {
        return "I am yours statistics service";
    }

    @GetMapping("/getEventByPostId/{id}")
    @Operation(summary = "Get event for post by id")
    public Event getEventByPostId(@PathVariable UUID id) {
        return eventService.getEventByPostId(id);
    }

    @GetMapping("/getEventByCommentId/{id}")
    @Operation(summary = "Get event for comment by id")
    public Event getEventByCommentId(@PathVariable UUID id) {
        return eventService.getEventByCommentId(id);
    }

    @GetMapping("/getEventBySubscriptionId/{id}")
    @Operation(summary = "Get event for subscription by id")
    public Event getEventBySubscriptionId(@PathVariable UUID id) {
        return eventService.getEventBySubscriptionId(id);
    }

    @PostMapping("/createEvent")
    @Operation(summary = "Get event for subscription by id")
    public String createEvent(@RequestBody Event event) {
        eventService.createEvent(event);
        return "Event processed";
    }
}
