package com.skillbox.statistics;

import com.skillbox.statistics.config.AppKafkaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@EnableConfigurationProperties(AppKafkaProperties.class)
public class StatisticsApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(StatisticsApplication.class, args);
        } catch (Throwable t) {
            System.out.println(t.getMessage());
            t.printStackTrace();
        }
    }
}
