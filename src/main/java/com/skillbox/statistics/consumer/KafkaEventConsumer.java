package com.skillbox.statistics.consumer;

import com.skillbox.statistics.domain.Event;
import com.skillbox.statistics.dto.EventRecord;
import com.skillbox.statistics.service.EventService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaEventConsumer {
    private final EventService eventService;
    public static final String TOPIC = "statistics";
    public static final String GROUP_ID = "statistics-events";

    @KafkaListener(groupId = GROUP_ID, topics = TOPIC)
    @SneakyThrows
    public void receiveKafkaMessage (@Payload @Validated EventRecord message) {
        Event event = new Event();
        event.setUuid(message.uuid());
        event.setEventInitiator(message.eventInitiator());
        event.setObjectId(message.objectId());
        event.setObjectType(message.objectType());
        event.setDateTime(message.dateTime());
        log.info("Прочитано сообщение {}", message);
        eventService.createEvent(event);
    }
}
