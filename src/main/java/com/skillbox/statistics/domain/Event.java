package com.skillbox.statistics.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document(collection = "events")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Event {
    @Id
    private UUID uuid;
    private LocalDateTime dateTime;
    private String objectType;
    private String objectId;
    private String eventInitiator;
}
